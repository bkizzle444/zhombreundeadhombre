// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerVariables.h"
#include "PlayerAnimations.h"


// Sets default values for this component's properties
UPlayerVariables::UPlayerVariables()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}

void UPlayerVariables::SetPlayerAnimation(UPlayerAnimations* AnimationToSet, UPaperFlipbookComponent* SpriteToSet, UStaticMeshComponent* CameraToSet)
{
	// Set components
	MyAnimations = AnimationToSet;
	MySprite = SpriteToSet;	
	MyCam = CameraToSet;
	// break vector
	FVector PlayerLocation = MySprite->GetOwner()->GetActorLocation();
	float PlayerX = PlayerLocation.X;
	float PlayerY = PlayerLocation.Y;
	float PlayerZ = PlayerLocation.Z;

	// Set cam Starting Location
	MyCam->SetWorldLocation(FVector(PlayerX, PlayerY, PlayerZ + CamHeight), false);
}

// tick
void UPlayerVariables::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction * ThisTickFunction)
{
	// set player 
	AActor* MyPlayer = MySprite->GetOwner();
	// break Player vector
	FVector PlayerLocation = MyPlayer->GetActorLocation();
	float PlayerX = PlayerLocation.X;
	float PlayerY = PlayerLocation.Y;
	float PlayerZ = PlayerLocation.Z;
	// sprite scale
	float PlayerScale = MySprite->GetComponentScale().X;
	// break Cam vector
	FVector CamLocation = MyCam->GetComponentLocation();
	float CamX = CamLocation.X;
	float Camy = CamLocation.Y;
	float CamZ = CamLocation.Z;

	// MoveCam
	if ((CamLeadPlayerDistance * PlayerScale) + PlayerX > CamX && PlayerScale == 1 || 
		(CamLeadPlayerDistance * PlayerScale) + PlayerX < CamX && PlayerScale == -1)
	{
		FMath::FInterpTo(CamX, (CamX + (PlayerScale *CameraMaxDistanceSpeed) + (MyPlayer->GetVelocity().X *.05f)), 0, 0);
	}
	
}


