// Fill out your copyright notice in the Description page of Project Settings.

#include "PlayerAnimations.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "PlayerVariables.h"


void UPlayerAnimations::SetupAnimFunc(UPlayerVariables* VariablesToSet, UPaperFlipbookComponent* SpriteToSet, UCharacterMovementComponent* MyCharacterToSet)
{
	MyVariables = VariablesToSet;
	MySprite = SpriteToSet;
	MyCharacter = MyCharacterToSet;
}


void UPlayerAnimations::PickAnimations()
{
	if (MyVariables->IsWalking == true)
	{
		MySprite->SetFlipbook(RunAnimation);
		MySprite->SetPlayRate(RunSpeed);
	}
}

void UPlayerAnimations::SelectEnum(EOutResult Branches, float VelocityX, float VelocityZ, int32 JumpAmount)
{

	bool IsOnGround = MyCharacter->IsMovingOnGround() == true;
	float AxisY = AxisValueTemp;

	if (Branches == EOutResult::Idle)
	{
		MySprite->SetFlipbook(IdleAnimation);
		MySprite->SetPlayRate(IdleSpeed);
		MySprite->Play();
		StartJump = false;
	}

	if (Branches == EOutResult::Run)
	{
		MySprite->SetFlipbook(RunAnimation);
		MySprite->SetPlayRate(RunSpeed);
		MySprite->Play();
		StartJump = false;
		MeleeCombo = 0;
	}

	// Jump
	if (VelocityZ > 0 && MyVariables->IsMelee == false)
	{
		if (Branches == EOutResult::Jump && JumpAmount == 1 && StartJump == false)
		{
			MySprite->SetFlipbook(JumpAnimation);
			MySprite->SetPlayRate(JumpSpeed);
			MySprite->Play();
			StartJump = true;
		}

		if (Branches == EOutResult::Jump && JumpAmount == 1 && MySprite->GetPlaybackPositionInFrames() >= 15 && StartJump == true)
		{
			MySprite->SetFlipbook(JumpAnimation);
			MySprite->SetPlayRate(JumpSpeed);
			MySprite->Play();
		}

		if (Branches == EOutResult::Jump && JumpAmount == 2)
		{
			MySprite->SetFlipbook(Jump2Animation);
			MySprite->SetPlayRate(Jump2Speed);
			MySprite->Play();
			if (MySprite->GetPlaybackPositionInFrames() >= 16 && MyVariables->IsMelee == false)
			{
				MySprite->Stop();
			}
		}
	}

	// Fall
	if (VelocityZ < 0 && MyVariables->IsMelee == false && JumpAmount != 2 || JumpAmount == 2 && MySprite->GetPlaybackPositionInFrames() == 16 && MyVariables->IsMelee == false)
	{
		MySprite->SetFlipbook(FallAnimation);
		MySprite->SetPlayRate(FallSpeed);
		MySprite->Play();
	}

	// Melee 1
	if (IsOnGround == true && AxisY < 1)
	{
		if (Branches == EOutResult::Melee && (MeleeCombo == 1 || MeleeCombo == 0))
		{
			if (MySprite->GetPlaybackPositionInFrames() < 15)
			{
				MySprite->SetFlipbook(MeleeAnimation);
				MySprite->SetPlayRate(MeleeSpeed);
				MySprite->Play();

				if (GetOwner()->GetWorldTimerManager().IsTimerActive(MeleeTimer) == false)
				{
					GetOwner()->GetWorldTimerManager().SetTimer(MeleeTimer, this, &UPlayerAnimations::MeleeTimerReset, MyVariables->MeleeGlobalCD, false);
					UE_LOG(LogTemp, Warning, TEXT("Start"))
				}

				if (GetOwner()->GetWorldTimerManager().IsTimerActive(MeleeTimer) == true && TimerReset == true)
				{
					GetOwner()->GetWorldTimerManager().ClearTimer(MeleeTimer);
					GetOwner()->GetWorldTimerManager().SetTimer(MeleeTimer, this, &UPlayerAnimations::MeleeTimerReset, MyVariables->MeleeGlobalCD, false);
					TimerReset = false;
					UE_LOG(LogTemp, Warning, TEXT("RESET"))
				}
			}

			if (MySprite->GetPlaybackPositionInFrames() >= 15)
			{
				MySprite->Stop();
				MyVariables->IsMelee = false;
			}

		}

		// Melee 2
		if (Branches == EOutResult::Melee && MeleeCombo == 2)
		{
			if (MeleeCombo == 2 && MySprite->GetPlaybackPositionInFrames() < 15)
			{
				MySprite->SetFlipbook(Melee2Animation);
				MySprite->SetPlayRate(Melee2Speed);
				MySprite->Play();

				if (GetOwner()->GetWorldTimerManager().IsTimerActive(MeleeTimer) == true && TimerReset == true)
				{
					GetOwner()->GetWorldTimerManager().ClearTimer(MeleeTimer);
					GetOwner()->GetWorldTimerManager().SetTimer(MeleeTimer, this, &UPlayerAnimations::MeleeTimerReset, MyVariables->MeleeGlobalCD, false);
					TimerReset = false;
					UE_LOG(LogTemp, Warning, TEXT("RESET"))
				}
			}

			if (MeleeCombo == 2 && MySprite->GetPlaybackPositionInFrames() >= 15)
			{
				MySprite->Stop();
				MyVariables->IsMelee = false;
			}

		}

		// Melee 3
		if (Branches == EOutResult::Melee && MeleeCombo == 3)
		{
			if (MySprite->GetPlaybackPositionInFrames() < 15)
			{
				MySprite->SetFlipbook(Melee3Animation);
				MySprite->SetPlayRate(Melee3Speed);
				MySprite->Play();

				if (GetOwner()->GetWorldTimerManager().IsTimerActive(MeleeTimer) == false && TimerReset == true)
				{
					GetOwner()->GetWorldTimerManager().ClearTimer(MeleeTimer);
					GetOwner()->GetWorldTimerManager().SetTimer(MeleeTimer, this, &UPlayerAnimations::MeleeTimerReset, MyVariables->MeleeGlobalCD, false);
					TimerReset = false;
					UE_LOG(LogTemp, Warning, TEXT("RESET"))
				}
			}

			if (MySprite->GetPlaybackPositionInFrames() >= 15)
			{
				MySprite->Stop();
				MyVariables->IsMelee = false;
				MeleeCombo = 0;
				GetOwner()->GetWorldTimerManager().ClearTimer(MeleeTimer);
			}
		}
	}

	// Melee up
	if (IsOnGround == true && AxisY == 1)
	{
		if (Branches == EOutResult::Melee)
		{
			MySprite->SetFlipbook(MeleeUpAnimation);
			MySprite->SetPlayRate(MeleeUpSpeed);
			MySprite->Play();
		}

		if (MySprite->GetPlaybackPositionInFrames() >= 15)
		{
			MySprite->Stop();
			MyVariables->IsMelee = false;
			MeleeCombo = 0;
		}
	}
	// AIR Melee
	if (IsOnGround != true && AxisY >= -.75)
	{
		// air melee 1
		if (Branches == EOutResult::Melee && (MeleeCombo == 1 || MeleeCombo == 0))
		{
			if (MySprite->GetPlaybackPositionInFrames() < 15)
			{
				MySprite->SetFlipbook(MeleeAir1Animation);
				MySprite->SetPlayRate(MeleeAir1Speed);
				MySprite->Play();

				if (GetOwner()->GetWorldTimerManager().IsTimerActive(MeleeTimer) == false)
				{
					GetOwner()->GetWorldTimerManager().SetTimer(MeleeTimer, this, &UPlayerAnimations::MeleeTimerReset, MyVariables->MeleeGlobalCD, false);
					UE_LOG(LogTemp, Warning, TEXT("Start"))
				}

				if (GetOwner()->GetWorldTimerManager().IsTimerActive(MeleeTimer) == true && TimerReset == true)
				{
					GetOwner()->GetWorldTimerManager().ClearTimer(MeleeTimer);
					GetOwner()->GetWorldTimerManager().SetTimer(MeleeTimer, this, &UPlayerAnimations::MeleeTimerReset, MyVariables->MeleeGlobalCD, false);
					TimerReset = false;
					UE_LOG(LogTemp, Warning, TEXT("RESET"))
				}
			}

			if (MySprite->GetPlaybackPositionInFrames() >= 15)
			{
				MySprite->Stop();
				MyVariables->IsMelee = false;
			}

		}

		// air melee 2
		if (Branches == EOutResult::Melee && MeleeCombo == 2)
		{
			if (MeleeCombo == 2 && MySprite->GetPlaybackPositionInFrames() < 15)
			{
				MySprite->SetFlipbook(Melee2Animation);
				MySprite->SetPlayRate(Melee2Speed);
				MySprite->Play();

				if (GetOwner()->GetWorldTimerManager().IsTimerActive(MeleeTimer) == true && TimerReset == true)
				{
					GetOwner()->GetWorldTimerManager().ClearTimer(MeleeTimer);
					GetOwner()->GetWorldTimerManager().SetTimer(MeleeTimer, this, &UPlayerAnimations::MeleeTimerReset, MyVariables->MeleeGlobalCD, false);
					TimerReset = false;
					UE_LOG(LogTemp, Warning, TEXT("RESET"))
				}
			}

			if (MeleeCombo == 2 && MySprite->GetPlaybackPositionInFrames() >= 15)
			{
				MySprite->Stop();
				MyVariables->IsMelee = false;
				MeleeCombo = 0;
				GetOwner()->GetWorldTimerManager().ClearTimer(MeleeTimer);
			}

		}

	}

	// downhit
		if (IsOnGround == false && AxisY <= -.75)
		{
			if (Branches == EOutResult::Melee)
			{
				MySprite->SetFlipbook(MeleeDownAnimation);
				MySprite->SetPlayRate(MeleeDownSpeed);
				MySprite->Play();
			}

			if (MySprite->GetPlaybackPositionInFrames() >= 15)
			{
				MySprite->Stop();
				MyVariables->IsMelee = false;
				MeleeCombo = 0;
			}
		}
}

void UPlayerAnimations::MeleeTimerReset()
{
	if (MyVariables->IsMelee == false)
	{
		MeleeCombo = 0;
		UE_LOG(LogTemp, Warning, TEXT("DONE"))
		TimerReset = false;
	}
}