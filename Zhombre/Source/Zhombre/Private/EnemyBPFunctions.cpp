// Fill out your copyright notice in the Description page of Project Settings.

#include "EnemyBPFunctions.h"
#include "EnemyVariables.h"


// Sets default values for this component's properties
UEnemyBPFunctions::UEnemyBPFunctions()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = false;

	// ...
}

void UEnemyBPFunctions::SetCallableFunctions(UEnemyVariables* MyVariablesToSet, UPaperFlipbookComponent* SpriteToSet)
{
	MyVariables = MyVariablesToSet;
	MySprite = SpriteToSet;
}

void UEnemyBPFunctions::BounceJumpLerp(AActor* TempBounce, FVector PlayerLocation, float BounceTimeLine)
{
	if (TempBounce != nullptr && MyVariables->EnemyBounceValue == 1)
	{
		FVector EnemyToJumpOnLocation = TempBounce->GetActorLocation();
		FVector BounceJumperLocation = GetOwner()->GetActorLocation();
		FVector EndLocation = FVector(EnemyToJumpOnLocation.X, BounceJumperLocation.Y, EnemyToJumpOnLocation.Z);
		FVector JumpToLerp = FMath::Lerp(BounceJumperLocation, EndLocation, BounceTimeLine);
		GetOwner()->SetActorLocation(JumpToLerp);
	}

	if (MyVariables->EnemyBounceValue == 2)
	{
		FVector BounceJumperLocation = GetOwner()->GetActorLocation();
		FVector EndLocation = FVector(PlayerLocation.X, BounceJumperLocation.Y, BounceJumperLocation.Z);
		FVector JumpToLerp = FMath::Lerp(BounceJumperLocation, EndLocation, BounceTimeLine);
		GetOwner()->SetActorLocation(JumpToLerp);
	}
}

void UEnemyBPFunctions::EnemyAttack()
{
	if (MyVariables->IsAttacking == false)
	{
		MyVariables->IsAttacking = true;
	}
}