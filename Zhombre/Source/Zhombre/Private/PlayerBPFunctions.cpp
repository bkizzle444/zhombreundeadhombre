// Fill out your copyright notice in the Description page of Project Settings.

#include "PlayerBPFunctions.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/Character.h"
#include "PlayerVariables.h"
#include "Zhombre.h"
#include "PlayerAnimations.h"

// Sets default values for this component's properties
UPlayerBPFunctions::UPlayerBPFunctions()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = false;

	// ...
}

void UPlayerBPFunctions::SetPlayer(UPlayerVariables* MyVariablesToSet, AActor* ActorToSet, UPlayerAnimations* MyAnimationsToSet, UPaperFlipbookComponent* SpriteToSet)
{
	//Set Pointers
	MyVariables = MyVariablesToSet;
	MyPlayer = ActorToSet;
	MyAnimations = MyAnimationsToSet;
	MySprite = SpriteToSet;
}


void UPlayerBPFunctions::SetMovement(float AxisValue, bool CanRun, bool Dodging, bool Burrowing, bool HittingMap, int32 JumpAmount)
{
	APlayerController* PlayerController = GetWorld()->GetFirstPlayerController();
	float Velocity = PlayerController->GetCharacter()->GetVelocity().X;
	float WorldScale = MySprite->GetComponentScale().X;

	bool IsGrounded = PlayerController->GetCharacter()->GetCharacterMovement()->IsMovingOnGround();
	
	if (Dodging == false)
	{
		if (CanRun == true)
		{
			PlayerController->GetCharacter()->AddMovementInput(FVector(2, 0, 0), AxisValue);
		}

		if (CanRun == false && MyVariables->IsMelee == false)
		{

			if (Burrowing == true && HittingMap == true)
			{
				PlayerController->GetCharacter()->AddMovementInput(FVector(.5, 0, 0), AxisValue);
			}
		}

		if (CanRun == false && MyVariables->IsMelee == true && IsGrounded == true)
		{
			PlayerController->GetCharacter()->AddMovementInput(FVector(.0015, 0, 0), AxisValue);
		}

		if (CanRun == false && MyVariables->IsMelee == true && IsGrounded == false)
		{
			PlayerController->GetCharacter()->AddMovementInput(FVector(.5, 0, 0), AxisValue);
		}



		// Scale 
		if (AxisValue > 0 && Velocity > 15 && JumpAmount < 2)
		{
			if (WorldScale != 1)
			{
				MySprite->SetRelativeScale3D(FVector(1, 1, 1));
			}
		}

		if (AxisValue < 0 && Velocity < 15 && JumpAmount < 2)
		{
			if (WorldScale != -1)
			{
				MySprite->SetRelativeScale3D(FVector(-1, 1, 1));
			}
		}

	}


}


// BodyThrow
void UPlayerBPFunctions::LerpBodyThrow(const float TimeL)
{
	float AxisX = TempAxisBodyThrow;
	// Set player location and break vector
	FVector PlayerStart = MyPlayer->GetActorLocation();
	float PlayerX = PlayerStart.X;
	float PlayerY = PlayerStart.Y;
	float PlayerZ = PlayerStart.Z;
	// Get player scale and set player end location
	float PlayerScale = MyVariables->MySprite->GetComponentScale().X;

	if (TempAxisBodyThrow != 0)
	{
		FVector PlayerEnd = FVector(PlayerX + (MyVariables->DodgeSpeed * TempAxisBodyThrow), PlayerY, PlayerZ);
		// Lerp for body throw
		FVector BodyThrowLerp = FMath::Lerp(PlayerStart, PlayerEnd, TimeL);
		MyPlayer->SetActorLocation(BodyThrowLerp);
	}

	if (TempAxisBodyThrow == 0)
	{
		FVector PlayerEnd = FVector(PlayerX + (MyVariables->DodgeSpeed * PlayerScale), PlayerY, PlayerZ);
		// Lerp for body throw
		FVector BodyThrowLerp = FMath::Lerp(PlayerStart, PlayerEnd, TimeL);
		MyPlayer->SetActorLocation(BodyThrowLerp);
	}
	
}

// Climb Over Wall
void UPlayerBPFunctions::LerpClimbOverWall(const float TimeL)
{
	// Set player location and break vector
	FVector PlayerStart = MyPlayer->GetActorLocation();
	float PlayerX = PlayerStart.X;
	float PlayerY = PlayerStart.Y;
	float PlayerZ = PlayerStart.Z;
	// Get player scale and set player end location
	float PlayerScale = MyVariables->MySprite->GetComponentScale().X;

	FVector PlayerEnd = FVector(PlayerX + (PlayerScale * 5), PlayerY, PlayerZ + 2);

	// Lerp for WallClimbOver
	FVector WallClimbOver = FMath::Lerp(PlayerStart, PlayerEnd, TimeL);

	//Set actor location
	MyPlayer->SetActorLocation(WallClimbOver);
}

// Grapple
void UPlayerBPFunctions::LerpGrappleActor(const float TimeL, AActor* CloseGrap)
{
	FVector PlayerStart = MyPlayer->GetActorLocation();
	FVector GrapLocation = CloseGrap->GetActorLocation();
	float GrapX = GrapLocation.X;
	float GrapY = GrapLocation.Y;
	float GrapZ = GrapLocation.Z;
	float PlayerScale = MyVariables->MySprite->GetComponentScale().X;
	FVector GrapVector = FVector(GrapLocation.X + (35 * PlayerScale), GrapLocation.Y, GrapLocation.Z + 250);

	
	FVector LerpToGrap = FMath::Lerp(PlayerStart, GrapVector, TimeL);

	if (MyVariables->IsGrappling)
	{
		MyPlayer->SetActorLocation(LerpToGrap);
	}

}

// Jump
void UPlayerBPFunctions::JumpLerp(const float TimeL, bool WallR, bool WallL)
{
	// Player Location
	FVector PlayerLoc = MyPlayer->GetActorLocation();

	// Break Vector
	float PlayerX = (PlayerLoc.X);
	float PlayerY = (PlayerLoc.Y);
	float PlayerZ = (PlayerLoc.Z);

	// check to players left/right
	FVector TargetXL = FVector(PlayerX - 100, PlayerY, PlayerZ);
	FVector TargetXR = FVector(PlayerX + 100, PlayerY, PlayerZ);

	// Lerp From Right Wall
	if (WallR)
	{
		FVector PlayerLerpX = FMath::Lerp(TargetXL, PlayerLoc, TimeL);
		MyPlayer->SetActorLocation((PlayerLerpX));
	}

	// Lerp From Left Wall
	if (WallL)
	{
		FVector PlayerLerpX = FMath::Lerp(TargetXR, PlayerLoc, TimeL);
		MyPlayer->SetActorLocation((PlayerLerpX));
	}


	// reset combo
	MyAnimations->MeleeCombo = 0;
}

// WallClimb
void UPlayerBPFunctions::WallClimbUp(const float TimeL, float AxisValue, const bool Zomrage)
{
	// Find player and setup up vector and lerp finish vector
	FVector PlayerStart = MyPlayer->GetActorLocation();
	FVector PlayerUp = MyPlayer->GetActorUpVector();

	// check for zomrage and increase timeline speed if so
	float ZomrageValue;
	if (Zomrage == true)
	{
		ZomrageValue = 4;
	}
	else { ZomrageValue = 2; }

	FVector PlayerEnd = PlayerStart + (PlayerUp * (AxisValue * ZomrageValue));

	// lerp and set actor location
	FVector TimeLineClimb = FMath::Lerp(PlayerStart, PlayerEnd, TimeL);
	MyPlayer->SetActorLocation(TimeLineClimb);

	//UE_LOG(LogTemp, Warning, TEXT("%f"), ZomrageValue)
}

// Zomrage
void UPlayerBPFunctions::ZomRage(float & DoubleMS, float & DoubleAccel, float & TempMoveSpeed, float & TempAccel)
{
	// Assign current values to temp variables
	TempMoveSpeed = MyVariables->MoveSpeed;
	TempAccel = MyVariables->acceleration;

	// Declare speed buff variables while raged
	DoubleMS = TempMoveSpeed * 1.5;
	DoubleAccel = TempAccel * 1.5;
}


// KnockBacks
void UPlayerBPFunctions::PlayerKnockBackMelee(float KnockBackTL, AActor* EnemyKnocked)
{

	FVector PlayerStart = MyPlayer->GetActorLocation();
	// Hit Enemy During Melee
	if (MyVariables->IsDodging == false && MyVariables->IsMelee == true)
	{
		float PlayerX = PlayerStart.X;
		float PlayerY = PlayerStart.Y;
		float PlayerZ = PlayerStart.Z;
		float PlayerScale = MyVariables->MySprite->GetComponentScale().X;
		FVector PlayerEnd = FVector(PlayerX + (PlayerScale * -50), PlayerY, PlayerZ);
		FVector TimeLineKnockBack = FMath::Lerp(PlayerStart, PlayerEnd, KnockBackTL);

		MyPlayer->SetActorLocation(TimeLineKnockBack);
	}

}

// Hit Enemy During BodyThrow
void UPlayerBPFunctions::PlayerKnockBackBodyThrow(UPaperFlipbookComponent* MySprite, FVector& OutVector)
{

	FVector PlayerForward = MyPlayer->GetActorForwardVector();
	float FacingDirection = MySprite->GetComponentScale().X;
	if (FacingDirection == 1 || FacingDirection == -1)
	{
		OutVector = (FVector(2500, 0, 0) * FacingDirection) + FVector(0, 0, 1500);
	}
	else
	{
		OutVector = FVector(0, 0, 0);
	}
	
	
}