// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "cmath"
#include "PlayerBPFunctions.generated.h"

class UPlayerVariables;
class UPlayerAnimations;

UCLASS(ClassGroup=(Custom), meta=(BlueprintSpawnableComponent), HideCategories = ("Variable", "Collision", "Component Replication", "Activation", "Cooking", "Tags"))
class ZHOMBRE_API UPlayerBPFunctions : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UPlayerBPFunctions();

protected:

public:

	//Set Player func + pointers
	UFUNCTION(BlueprintCallable)
	void SetPlayer(UPlayerVariables* MyVariablesToSet, AActor* ActorToSet, UPlayerAnimations* MyAnimationsToSet, UPaperFlipbookComponent* SpriteToSet);

	// SetMovement IDLE/WALK
	UFUNCTION(BlueprintCallable)
	void SetMovement(float AxisValue, bool CanRun, bool Dodging, bool Burrowing, bool HittingMap, int32 JumpAmount);

	// Lerp body throw
	UFUNCTION(BlueprintCallable)
	void LerpBodyThrow(const float TimeL);

	// Lerp climb over wall
	UFUNCTION(BlueprintCallable)
	void LerpClimbOverWall(const float TimeL);

	// Lerp way to grapple actor
	UFUNCTION(BlueprintCallable)
	void LerpGrappleActor(const float TimeL, AActor* CloseGrap);

	// Lerp Jump Off Wall
	UFUNCTION(BlueprintCallable)
	void JumpLerp(const float TimeL, bool WallR, bool WallL);

	// Lerp Jump WallClimb up
	UFUNCTION(BlueprintCallable)
	void WallClimbUp(const float TimeL, float AxisValue, const bool ZomrageValue);

	// Control Zomrage Buff
	UFUNCTION(BlueprintCallable)
	void ZomRage(float& DoubleMS, float& DoubleAccel, float& TempMoveSpeed, float& TempAccel);

	// Knockback
	UFUNCTION(BlueprintCallable)
	void PlayerKnockBackMelee(float KnockBackTL, AActor* EnemyKnocked);

	UFUNCTION(BlueprintCallable)
	void PlayerKnockBackBodyThrow(UPaperFlipbookComponent* MySprite, FVector& OutVector);

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	float TempAxisBodyThrow;

	// Pointers
	UPlayerAnimations* MyAnimations;
	UPlayerVariables* MyVariables;
	AActor* MyPlayer;
	UPaperFlipbookComponent* MySprite;
};

