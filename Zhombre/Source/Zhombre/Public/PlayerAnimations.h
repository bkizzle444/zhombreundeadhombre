// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "PaperFlipbookComponent.h"
#include "PlayerAnimations.generated.h"

class UPlayerVariables;

UENUM(BlueprintType)
enum class EOutResult : uint8
{
	Idle,
	Run,
	Jump,
	Melee,
	Dodge,
	WallClimb,
	Grapple,
	LifeDrain
};

/**
 * 
 */
UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent), HideCategories = ("Rendering", "Transform", "Sprite", "Variable", "Materials", "Physics", "Lighting", "Collision", "Tags", "Cooking", "Activation"))
class ZHOMBRE_API UPlayerAnimations : public UPaperFlipbookComponent
{
	GENERATED_BODY()

	public:

	// Idle
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Player Idle")
	UPaperFlipbook* IdleAnimation;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Player Idle")
	float IdleFrames;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Player Idle")
	float IdleSpeed = 1;

	// Run
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Player Run")
	UPaperFlipbook* RunAnimation;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Player Run")
	float RunFrames;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Player Run")
	float RunSpeed = 1;


	// Start Jump
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Player JumpStart")
	UPaperFlipbook* JumpStartAnimation;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Player JumpStart")
	float JumpStartFrames;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Player JumpStart")
	float JumpStartSpeed = 1;

	// Jump
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Player Jump")
	UPaperFlipbook* JumpAnimation;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Player Jump")
	float JumpFrames;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Player Jump")
	float JumpSpeed = 1;

	// DBJ 
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Player 2xJump")
	UPaperFlipbook* Jump2Animation;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Player 2xJump")
	float Jump2Frames;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Player 2xJump")
	float Jump2Speed = 1;

	// End Jump
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Player JumpEnd")
	UPaperFlipbook* JumpEndAnimation;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Player JumpEnd")
	float JumpEndFrames;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Player JumpEnd")
	float JumpEndSpeed = 1;

	// Fall
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Player Fall")
	UPaperFlipbook* FallAnimation;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Player Fall")
	float FallFrames;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Player Fall")
	float FallSpeed = 1;

	// WallClimb
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Player WallClimb")
	UPaperFlipbook* WallClimbAnimation;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Player WallClimb")
	float WallClimbFrames;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Player WallClimb")
	float WallClimbSpeed = 1;

	// Melee1
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Player Melee")
	UPaperFlipbook* MeleeAnimation;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Player Melee")
	float MeleeFrames;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Player Melee")
	float MeleeSpeed = 1;

	// Melee2
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Player Melee2")
	UPaperFlipbook* Melee2Animation;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Player Melee2")
	float Melee2Frames;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Player Melee2")
	float Melee2Speed = 1;

	// Melee3
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Player Melee3")
	UPaperFlipbook* Melee3Animation;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Player Melee3")
	float Melee3Frames;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Player Melee3")
	float Melee3Speed = 1;

	// MeleeAir1
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Player MeleeAir1")
	UPaperFlipbook* MeleeAir1Animation;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Player MeleeAir1")
	float MeleeAir1Frames;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Player MeleeAir1")
	float MeleeAir1Speed = 1;

	// MeleeAir1
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Player MeleeAir2")
	UPaperFlipbook* MeleeAir2Animation;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Player MeleeAir2")
	float MeleeAir2Frames;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Player MeleeAir2")
	float MeleeAir2Speed = 1;

	// MeleeDown
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Player MeleeDown")
	UPaperFlipbook* MeleeDownAnimation;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Player MeleeDown")
	float MeleeDownFrames;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Player MeleeDown")
	float MeleeDownSpeed = 1;

	// MeleeUp
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Player MeleeUp")
	UPaperFlipbook* MeleeUpAnimation;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Player MeleeUp")
	float MeleeUpFrames;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Player MeleeUp")
	float MeleeUpSpeed = 1;

	// BodyThrow
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Player BodyThrow")
	UPaperFlipbook* BodyThrowAnimation;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Player BodyThrow")
	float BodyThrowFrames;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Player BodyThrow")
	float BodyThrowSpeed = 1;

	// Burrowdown
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Player Burrowdown")
	UPaperFlipbook* BurrowDown;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Player Burrowdown")
	float BurrowDownFrames;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Player Burrowdown")
	float BurrowDownSpeed = 1;

	// Burrowed
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Player Burrowed")
	UPaperFlipbook* Burrowed;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Player Burrowed")
	float BurrowedFrames;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Player Burrowed")
	float BurrowedSpeed = 1;

	// BurrowUp
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Player BurrowUp")
	UPaperFlipbook* BurrowUp;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Player BurrowUp")
	float BurrowUpFrames;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Player BurrowUp")
	float BurrowUpSpeed = 1;

	// Grapple
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Player Grapple")
	UPaperFlipbook* GrappleAnimation;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Player Grapple")
	float GrappleFrames;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Player Grapple")
	float GrappleSpeed = 1;

	// LifeDrain
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Player LifeDrain")
	UPaperFlipbook* LifeDrainAnimation;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Player LifeDrain")
	float LifeDrainFrames;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Player LifeDrain")
	float LifeDrainSpeed = 1;

	// LifeDrainUse
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Player LifeDrainUse")
	UPaperFlipbook* LifeDrainUseAnimation;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Player LifeDrainUse")
	float LifeDrainUseFrames;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Player LifeDrainUse")
	float LifeDrainUseSpeed = 1;

	// ZomRageForm (Switch)
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Player ZomRageFormSwitch")
	UPaperFlipbook* ZomRageFormSwitchAnimation;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Player ZomRageFormSwitch")
	float ZomRageFormSwitchFrames;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Player ZomRageFormSwitch")
	float ZomRageFormSwitchSpeed = 1;

	// ZomRageIdle
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Player ZomRageIdle")
	UPaperFlipbook* ZomRageIdleAnimation;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Player ZomRageIdle")
	float ZomRageIdleFrames;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Player ZomRageIdle")
	float ZomRageIdleSpeed = 1;

	// ZomRageRun
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Player ZomRageRun")
	UPaperFlipbook* ZomRageRunAnimation;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Player ZomRageRun")
	float ZomRageRunFrames;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Player ZomRageRun")
	float ZomRageRunSpeed = 1;

	// ZomRageMelee
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Player ZomRageMelee")
	UPaperFlipbook* ZomRageMeleeAnimation;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Player ZomRageMelee")
	float ZomRageMeleeFrames;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Player ZomRageMelee")
	float ZomRageMeleeSpeed = 1;

	// ZomRageJump
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Player ZomRageJump")
	UPaperFlipbook* ZomRageJumpAnimation;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Player ZomRageJump")
	float ZomRageJumpFrames;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Player ZomRageJump")
	float ZomRageJumpSpeed = 1;

	// ZomRageFall
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Player ZomRageFall")
	UPaperFlipbook* ZomRageFallAnimation;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Player ZomRageFall")
	float ZomRageFallFrames;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Player ZomRageFall")
	float ZomRageFallSpeed = 1;



	//Functions
	UFUNCTION(BlueprintCallable)
	void SetupAnimFunc(UPlayerVariables* VariablesToSet, UPaperFlipbookComponent* SpriteToSet, UCharacterMovementComponent* MyCharacterToSet);

	// AnimSetup
	UFUNCTION(BlueprintCallable)
	void PickAnimations();

	// EnumSetup
	UFUNCTION(BlueprintCallable, Category = "DataTable", meta = (ExpandEnumAsExecs = "Branches"))
	void SelectEnum(EOutResult Branches, float VelocityX, float VelocityZ, int32 JumpAmount);

	//
	bool StartJump = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 MeleeCombo;
	int32 AirMeleeCombo;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float AxisValueTemp;

	// timer
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool TimerReset;
	FTimerHandle MeleeTimer;
	void MeleeTimerReset();

	// Pointers
	UPlayerVariables* MyVariables;
	UPaperFlipbookComponent* MySprite;
	UCharacterMovementComponent* MyCharacter;
};
