// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "EnemyBPFunctions.generated.h"

class UEnemyVariables;


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class ZHOMBRE_API UEnemyBPFunctions : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UEnemyBPFunctions();

protected:


public:	

	UFUNCTION(BlueprintCallable)
	void SetCallableFunctions(UEnemyVariables* MyVariablesToSet, UPaperFlipbookComponent* SpriteToSet);

	UFUNCTION(BlueprintCallable)
	void BounceJumpLerp(AActor* TempBounce, FVector PlayerLocation, float BounceTimeLine);

	UFUNCTION(BlueprintCallable)
	void EnemyAttack();

		
	UEnemyVariables* MyVariables;
	UPaperFlipbookComponent* MySprite;
};
