// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "cmath"
#include "PlayerVariables.generated.h"

class UPlayerAnimations;

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent), HideCategories = ("Variable", "Collision", "Component Replication", "Activation", "Cooking", "Tags"))
class ZHOMBRE_API UPlayerVariables : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UPlayerVariables();

protected:
	// Called when the game starts
	//virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;


	// set player animation pointer
	UFUNCTION(BlueprintCallable)
	void SetPlayerAnimation(UPlayerAnimations* AnimationToSet, UPaperFlipbookComponent* SpriteToSet, UStaticMeshComponent* CameraToSet);

	// bools
	UPROPERTY(BlueprintReadWrite, Category = "Setup")
	bool IsIdle = false;
	UPROPERTY(BlueprintReadWrite, Category = "Setup")
	bool IsWalking = false;
	UPROPERTY(BlueprintReadWrite, Category = "Setup")
	bool IsDodging = false;
	UPROPERTY(BlueprintReadWrite, Category = "Setup")
	bool IsJumping = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setup")
	bool CanDodge = true;
	UPROPERTY(BlueprintReadWrite, Category = "Setup")
	bool IsMelee = false;
	UPROPERTY(BlueprintReadWrite, Category = "Setup")
	bool IsWallClimbing = false;
	UPROPERTY(BlueprintReadWrite, Category = "Setup")
	bool IsGrappling = false;
	UPROPERTY(BlueprintReadWrite, Category = "Setup")
	bool IsLifeDraining = false;
	UPROPERTY(BlueprintReadWrite, Category = "Setup")
	bool IsInjectingLifeDrain = false;
	UPROPERTY(BlueprintReadWrite, Category = "Setup")
	bool IsBurrow = false;
	UPROPERTY(BlueprintReadWrite, Category = "Setup")
	bool IsZomrage = false;
	UPROPERTY(BlueprintReadWrite, Category = "Setup")
	bool IsKnockBack = false;

	// Player Attack control variables
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Player Attack Variables")
	float MeleeGlobalCD;


	// designer player control variables
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Player Variables")
	float MoveSpeed;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Player Variables")
	float acceleration;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Player Variables")
	float PlayerGravity;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Player Variables")
	float JumpVelocity;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Player Variables")
	int32 JumpAmount;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Player Variables")
	float JumpHold;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Player Variables")
	float DodgeSpeed = 50;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Player Variables")
	float DodgeGravityValue = 3;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Player Variables")
	float DodgeCD;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Player Variables")
	float LifeDrainFillupSpeed = 2;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Player Variables")
	int32 CurrentDrainAmount = 0;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Player Variables")
	int32 MaxDrainsHeld = 5;


	// designer camera control variables
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Player Camera Variables")
	float CamStartDistanceFromPlayer = 2450.0;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Player Camera Variables")
	float CamLeadPlayerDistance = 400;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Player Camera Variables")
	float CamHeight = 550;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Player Camera Variables")
	float CameraMaxDistanceSpeed = 20;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Player Camera Variables")
	float CameraLagSpeed = 10;

	// Pointers
	UPlayerAnimations* MyAnimations;
	UPaperFlipbookComponent* MySprite;
	UStaticMeshComponent* MyCam;
};
